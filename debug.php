<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 27-10-17
 * Time: 10:12
 */

include_once "helpers/session.php";

if (isset($_SESSION['debug']) && $_SESSION['debug'] == 1) {
    $_SESSION['debug'] = 0;
} else {
    $_SESSION['debug'] = 1;
}

header('Location: index.php');
exit(0);