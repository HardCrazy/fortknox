<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 27-10-17
 * Time: 09:24
 */

/**
 * @param $file
 * @return string
 */
function asset($file) {
    return join(DIRECTORY_SEPARATOR, array(ROOT_DIR, 'assets', $file));
}

/**
 * @param array $data
 */
function clearEmpty(array &$data) {
    foreach ($data as $key => $value) {
        $value = trim($value);
        if ($value === '') {
            $data[$key] = null;
        }
    }
}

function clearParam($param) {
    if ($param === null) {
        return 'NULL';
    } else {
        return "'" . $param . "'";
    }
}
