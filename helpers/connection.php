<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 27-10-17
 * Time: 09:23
 */

$pdo = new PDO('mysql:dbname=fortknox;host=127.0.0.1;charset=UTF8', 'root', null);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);