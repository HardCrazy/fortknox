<?php include "header.php"; ?>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Liste des commentaires correspondant à la recherche
        </div>
        <div class="panel-body">
            <ul>
                <?php
                foreach ($comments as $comment) {
                    echo "<li><a href='index.php?page=comment&action=show&id=$comment->id'>$comment->author : $comment->content</a></li>";
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>
