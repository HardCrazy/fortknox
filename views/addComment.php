<?php include "header.php"; ?>

<div class="col-md-12">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Ajouter un commentaire</div>
            <div class="panel-body">
                <form action="" class="form-horizontal" method="POST">
                    <div class="col-md-12 form-group">
                        <input type="text" class="form-control" name="author" placeholder="Auteur" required value="<?= isset($userLoggedIn) ? $userLoggedIn->username : ''; ?>">
                    </div>
                    <div class="col-md-12 form-group">
                        <textarea name="content" id="content" cols="30" rows="10" class="form-control"
                                  placeholder="Contenu"
                                  required></textarea>
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="submit" class="btn btn-success" name="submit" value="Enregistrer">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>
