<?php include "header.php"; ?>

<div class="col-md-12">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Se connecter</div>
            <div class="panel-body">
                <form action="" class="form-horizontal" method="POST">
                    <?= !empty($_POST) ? '<div class="alert alert-danger"><p class="strong">Mauvais identifiants</p></div>' : ''; ?>
                    <div class="col-md-12 form-group">
                        <input type="text" class="form-control" name="username" placeholder="Nom d'utilisateur" required value="<?= isset($_POST['username']) ? $_POST['username'] : ''; ?>">
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="password" class="form-control" name="password" placeholder="Mot de passe" required>
                    </div>
                    <div class="col-md-12 form-group">
                        <input type="submit" class="btn btn-success" name="submit" value="Se conneter">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>
