<?php
include "header.php";
?>

<div class="col-md-12">
    <h1>Bienvenue sur la Homepage de FortKNOX</h1>
</div>
<?php

if (isset($_SESSION['message'])) {
    ?>
    <div class="col-md-12">
        <div class="alert alert-success"><?= $_SESSION['message']; ?></div>
    </div>
    <?php

    unset($_SESSION['message']);
}

if (isset($_SESSION['erreur'])) {
    ?>
    <div class="col-md-12">
        <div class="alert alert-danger"><?= $_SESSION['erreur']; ?></div>
    </div>
    <?php

    unset($_SESSION['erreur']);
}
?>

<div class="col-md-12">
    <h1>Liste des commentaires</h1>
    <ul>
        <?php
        foreach ($comments as $comment) {
            echo "<li><a href='index.php?page=comment&action=show&id=$comment->id'>$comment->author : $comment->content</a></li>";
        }
        ?>
    </ul>
</div>

<?php
include "footer.php"
?>
