<?php include "header.php"; ?>

<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading text-info text-uppercase">
            <?= $comment->author ?>
        </div>
        <div class="panel-body">
            <?= $comment->content ?>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>
