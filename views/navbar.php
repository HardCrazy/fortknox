<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php?page=home">FortKNOX</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="index.php?page=comment&action=add">Nouveau commentaire</a></li>
            </ul>
            <form class="navbar-form navbar-left" role="search" method="POST" action="index.php?page=search">
                <div class="form-group">
                    <input type="text" name="item" class="form-control" placeholder="Recherche ...">
                </div>
                <button type="submit" class="btn btn-default">Rechercher</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <?php
                if (isset($_SESSION['debug']) && $_SESSION['debug'] == 1) {
                    echo '<li><a href="debug.php"><strong>Debug enabled</strong></a></li>';
                }
                if ($userLoggedIn !== null) {
                    echo sprintf('<li><a>Bievenue <strong>%s</strong></a></li>', $userLoggedIn->username);
                    echo '<li><a href="index.php?page=logout">Se déconnecter</a></li>';
                } else {
                    echo '<li><a href="index.php?page=login">Se connecter</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>