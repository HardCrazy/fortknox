<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 27-10-17
 * Time: 09:01
 */

include_once "helpers/session.php";

define('ROOT_DIR', "./");

include_once 'helpers/connection.php';
include_once 'helpers/function.php';

$userLoggedIn = null;

if (isset($_SESSION['userID'])) {
    $query = sprintf("SELECT * FROM Users WHERE id = '%s'", $_SESSION['userID']);
    $stmt = $pdo->query($query);
    $userLoggedIn = $stmt->fetch();
}

if (isset($_GET['page'])) {
    switch ($_GET['page']) {
        case "login":
            include "controller/login.php";
            break;

        case "logout":
            include "controller/logout.php";
            break;

        case "comment":
            include "controller/comment.php";
            break;

        case "search":
            include "controller/search.php";
            break;

        default:
            include "controller/home.php";
            break;
    }

} else {
    include "controller/home.php";
}