<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 27-10-17
 * Time: 10:06
 */

session_destroy();
session_unset();

header('Location: index.php');
exit(0);