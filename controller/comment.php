<?php

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'add':
            if (!empty($_POST)) {
                clearEmpty($_POST);
                $query = sprintf("INSERT INTO Comments (author, content) VALUES (%s, %s)", clearParam($_POST['author']), clearParam($_POST['content']));
                $pdo->exec($query);

                $_SESSION['message'] = 'Commentaire correctement ajouté';

                header('Location: index.php');
                exit(0);
            }
            include "views/addComment.php";
            break;

        case 'show':
            if (isset($_GET['id'])) {
                $query = sprintf("SELECT * FROM Comments WHERE id = '%s'", $_GET['id']);
                $stmt = $pdo->query($query);
                $comment = $stmt->fetch();

                if ($comment === false) {
                    $_SESSION['erreur'] = 'Commentaire non trouvé';
                    header('Location: index.php');
                    exit(2);
                }

                include "views/showComment.php";
            } else {
                $_SESSION['erreur'] = 'Id du commentaire non trouvé';

                header('Location: index.php');
                exit(1);
            }
            break;
    }
} else {
    header('Location: index.php');
    exit(0);
}

